https://www.krista-writes.com/

You want some kickass writing!? Then you have come to the right person!

‘We sell a good night’s sleep and not the mattress’

Impressed by that liner? Well, you will have a lot more like them for your product as well.


Hi. This is Krista Glover for you. The writing lover and advertising enthusiast.


Let me get all the adjectives for myself here. I’m creative, intuitive, sensitive, articulate, and expressive. I’m definitely unstructured yet original, nonconforming and innovative.